/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ex_1;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 *
 * @author user
 */
public class ox {
    
    static ArrayList<Integer> Xlist = new ArrayList<Integer>();
    static ArrayList<Integer> Olist = new ArrayList<Integer>();
    
    public static void main(String[] args) {
        System.out.println("Welcome to OX Game");
        char [][] table = {{' ','1','2','3'},
            {'1','-','-','-'},
            {'2','-','-','-'},
            {'3','-','-','-'}};
          
        Scanner kb = new Scanner(System.in);
        while(true){  
        System.out.println("X turn");
        System.out.println("Please input Row Col:");
        int x = kb.nextInt();
         XO(table,x,"X");
         String result = checkWin();
           if(result.length()>0){
             System.out.println(result);
             break;
         }
         
     
         System.out.println("O turn");
        System.out.println("Please input Row Col:");
        int o = kb.nextInt();
        XO(table,o,"O");
         
         result = checkWin();
            if(result.length()>0){
             System.out.println(result);
             break;
         }
         
       
        }
       
    }
    
       public static void XO(char[][] table,int ch,String user){
           
           char symbol = ' ';
           if(user.equals("X")){
               symbol = 'X';
               Xlist.add(ch);
           }
           else if(user.equals("O")){
               symbol = 'O';
               Olist.add(ch);
           }
           
              switch(ch){
                case 1:
                    table[1][1] = symbol;
                    break;
                case 2:
                    table[1][2] = symbol;
                    break;
                case 3:
                    table[1][3] = symbol;
                    break;
                case 4:
                    table[2][1] = symbol;
                    break;
                case 5:
                    table[2][2] = symbol;
                    break;
                case 6:
                    table[2][3] = symbol;
                    break;
                case 7:
                    table[3][1] = symbol;
                    break;
                case 8:
                    table[3][2] = symbol;
                    break;
                case 9:
                    table[3][3] = symbol;
                    break;    
        }
           
        printTable(table);
               
    }
    
    
    
    
    public static void printTable(char[][] table){
          for(char[] row : table) {
        for(char c : row){
            System.out.print(c);
        }
            System.out.println();
    }
    }
    
    public static String checkWin(){
       List topRow = Arrays.asList(1, 2, 3);
       List midRow = Arrays.asList(4, 5, 6);
       List botRow = Arrays.asList(7, 8, 9);
       List leftc = Arrays.asList(1, 4, 7);
       List midc = Arrays.asList(2, 5, 8);
       List rightc = Arrays.asList(3, 6, 9);
       List cross1 = Arrays.asList(1, 5, 9);
       List cross2 = Arrays.asList(7, 5, 3);
       
       List<List> win = new ArrayList<List>();
       win.add(topRow);
       win.add(botRow);
       win.add(leftc);
       win.add(midc);
       win.add(rightc);
       win.add(cross1);
       win.add(cross2);
       
       for(List l : win){
           if(Xlist.containsAll(l)){
               return "Player X Win...\nBye bye...";
           }
           else if(Olist.containsAll(l)){
               return "Player O Win...\nBye bye...";
           }
           else if(Xlist.size()+Olist.size()==9){
               return "Draw...";
           }
       }
        return "";
       
       
    }
 }
        
    

